﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public int speed = 7;
    public Vector2 targetPosition;

    private bool isWalking = false;
    private bool isFacingLeft = false;
    private Animator playerAnimator;

    private Vector2 previousTargetPosition;
    void Start()
    {
        playerAnimator = gameObject.GetComponent<Animator>();
        previousTargetPosition = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPosition.y = gameObject.transform.position.y;
            isWalking = true;
        }

        if (isWalking)
        {
            gameObject.transform.position = Vector2.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);

            if (previousTargetPosition.x > targetPosition.x)
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
            if (previousTargetPosition.x < targetPosition.x)
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
            }
            previousTargetPosition = gameObject.transform.position;

            if (transform.position.x == targetPosition.x)
            {
                playerAnimator.SetBool("IsMoving", false);
                gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                isWalking = false;
            }
            else
            {
                playerAnimator.SetBool("IsMoving", true);
            }
            
            Debug.Log(targetPosition);
        }
    }
}
